# minecraft-play

Общие моды:

1. [Adorn](https://www.curseforge.com/minecraft/mc-mods/adorn) - [1.14.1 (01.06.2021)](https://www.curseforge.com/minecraft/mc-mods/adorn/files/all)  
Фурнитура.
1. [Apple Trees Revived](https://www.curseforge.com/minecraft/mc-mods/apple-trees-revived) - [fabric 2.1 (16.07.2021)](https://www.curseforge.com/minecraft/mc-mods/apple-trees-revived/files/all)  
Яблоки на деревьях и яблочные деревья.
1. [AppleSkin](https://www.curseforge.com/minecraft/mc-mods/appleskin) - [fabric 1.0.11 beta (24.06.2020)](https://www.curseforge.com/minecraft/mc-mods/appleskin/files/all)  
Худ, показывающий доп. эффекты хавки. Последняя версия, которая работает.
1. [Applied Energistics 2](https://www.curseforge.com/minecraft/mc-mods/applied-energistics-2) - [fabric 8.4.0 alpha 1 (07.07.2021)](https://www.curseforge.com/minecraft/mc-mods/applied-energistics-2/files/all)  
МЕ хранилище, механизмы.
1. \[L\] [Architectury API (Fabric)](https://www.curseforge.com/minecraft/mc-mods/architectury-fabric) - [1.20.28 (12.07.2021)](https://www.curseforge.com/minecraft/mc-mods/architectury-fabric/files/all)  
Либа для Bamboo Tweaks: Fabric и Shelf.
1. [Artifacts (Fabric)](https://www.curseforge.com/minecraft/mc-mods/artifacts-fabric) - [3.2.1 (10.05.2021)](https://www.curseforge.com/minecraft/mc-mods/artifacts-fabric/files/all)  
Артефакты.
1. [Bamboo Tweaks: Fabric](https://www.curseforge.com/minecraft/mc-mods/bamboo-tweaks-fabric) - [2.0.5 beta (11.04.2021)](https://www.curseforge.com/minecraft/mc-mods/bamboo-tweaks-fabric/files/all)  
Мелочёвка по типу дверей и калиток с бамбука.
1. \[L\] [BCLib](https://www.curseforge.com/minecraft/mc-mods/bclib) - [0.1.45 (04.07.2021)](https://www.curseforge.com/minecraft/mc-mods/bclib/files)  
Либа для BetterEnd.
1. [BetterEnd](https://www.curseforge.com/minecraft/mc-mods/betterend) - [0.9.8.5 (22.06.2021)](https://www.curseforge.com/minecraft/mc-mods/betterend/files/all)  
Новые биомы и разные штуки в эндер мире.
1. [BetterNether](https://www.curseforge.com/minecraft/mc-mods/betternether) - [5.0.7 (25.01.2021)](https://www.curseforge.com/minecraft/mc-mods/betternether/files/all)  
Новые биомы и разные штуки в аду.
1. [Crimson Moon](https://www.curseforge.com/minecraft/mc-mods/crimson-moon) - [2.0.4 (31.03.2021)](https://www.curseforge.com/minecraft/mc-mods/crimson-moon/files/all)  
Евент раз в 20 дней, представляющий опасность.
1. [Dehydration](https://www.curseforge.com/minecraft/mc-mods/dehydration) - [1.1.4 (16.05.2021)](https://www.curseforge.com/minecraft/mc-mods/dehydration/files/all)  
Жажда.
1. [EnvironmentZ](https://www.curseforge.com/minecraft/mc-mods/environmentz) - [1.1.1 (16.05.2021)](https://www.curseforge.com/minecraft/mc-mods/environmentz/files/all)  
Добавляет эффекты от температур.
1. [Extra Generators](https://www.curseforge.com/minecraft/mc-mods/extra-generators) - [1.0.3 beta (30.06.2021)](https://www.curseforge.com/minecraft/mc-mods/extra-generators/files/all)  
Добавляет разные генераторы для тех. модов.
1. \[L\] [Fabric Language Kotlin](https://www.curseforge.com/minecraft/mc-mods/fabric-language-kotlin) - [1.6.1 (31.05.2021)](https://www.curseforge.com/minecraft/mc-mods/fabric-language-kotlin/files/all)  
Либа для Adorn.
1. [Fabric Seasons](https://www.curseforge.com/minecraft/mc-mods/fabric-seasons) - [1.2.0.1 beta (15.06.2021)](https://www.curseforge.com/minecraft/mc-mods/fabric-seasons/files/all)  
Сезоны.
1. \[L\] [Fabric API](https://www.curseforge.com/minecraft/mc-mods/fabric-api) - [0.37.0 (12.07.2021)](https://www.curseforge.com/minecraft/mc-mods/fabric-api/files/all)  
Либа для практически всех модов на Fabric.
1. \[L\] [FlytreLib](https://www.curseforge.com/minecraft/mc-mods/lib) - [0.2.11 (28.05.2021)](https://www.curseforge.com/minecraft/mc-mods/lib/files)  
Либа для Mechanix.
1. \[L\] [GeckoLib](https://www.curseforge.com/minecraft/mc-mods/geckolib-fabric) - [3.0.42 (30.06.2021)](https://www.curseforge.com/minecraft/mc-mods/geckolib-fabric/files/all)  
Либа для Moon and Space Dimensions (Fabric).
1. [Gravestones](https://www.curseforge.com/minecraft/mc-mods/gravestones) - [1.9 (16.02.2021)](https://www.curseforge.com/minecraft/mc-mods/gravestones/files/all)  
Могилки.
1. [Inventory Sorting](https://www.curseforge.com/minecraft/mc-mods/inventory-sorting) - [1.7.9 (17.04.2021)](https://www.curseforge.com/minecraft/mc-mods/inventory-sorting/files/all)  
Сортировка инвентаря.
1. \[L\] [Induim](https://github.com/comp500/Indium) - [1.0.0 (19.07.2021)](https://github.com/comp500/Indium/releases)  
Дополнение для Sodium, который позволяет работать Industrial Revolution.
1. [Industrial Revolution](https://www.curseforge.com/minecraft/mc-mods/industrial-revolution) - [1.9.18 beta (25.06.2021)](https://www.curseforge.com/minecraft/mc-mods/industrial-revolution/files/all)  
Технический мод.
1. [[Fabric] Iron Chests](https://www.curseforge.com/minecraft/mc-mods/iron-chests-fabric) - [1.2 (06.12.2020)](https://www.curseforge.com/minecraft/mc-mods/iron-chests-fabric/files/all)  
Улучшения для сундуков.
1. [Krypton](https://www.curseforge.com/minecraft/mc-mods/krypton) - [0.1.2 (11.02.2021)](https://www.curseforge.com/minecraft/mc-mods/krypton/files/all)  
Оптимизация сетевого стека.
1. [Lithium (Fabric)](https://www.curseforge.com/minecraft/mc-mods/lithium) - [0.6.6 (09.06.2021)](https://www.curseforge.com/minecraft/mc-mods/lithium/files/all)  
Оптимизация сервера.
1. [Lifts](https://www.curseforge.com/minecraft/mc-mods/lifts) - [1.1.1 (06.05.2021)](https://www.curseforge.com/minecraft/mc-mods/lifts/files/all)  
Лифты.
1. [MC Dungeons Weapons](https://www.curseforge.com/minecraft/mc-mods/mcdw) - [3.2.0 (04.06.2021)](https://www.curseforge.com/minecraft/mc-mods/mcdw/files/all)  
Дополнительное оружие.
1. [Mechanix](https://www.curseforge.com/minecraft/mc-mods/mechanix) - [5.0.0 (28.05.2021)](https://www.curseforge.com/minecraft/mc-mods/mechanix/files/all)  
Аддон к Tech Reborn.
1. [Modern Glass Doors](https://www.curseforge.com/minecraft/mc-mods/modern-glass-doors) - [1.5.0 (27.09.2020)](https://www.curseforge.com/minecraft/mc-mods/modern-glass-doors/files/all)  
Красивые двери и люки.
1. [[FABRIC] Monster Of The Ocean Depths](https://www.curseforge.com/minecraft/mc-mods/monster-of-the-ocean-depths-fabric) - [3.1 (01.03.2021)](https://www.curseforge.com/minecraft/mc-mods/monster-of-the-ocean-depths-fabric/files/all)  
Водная поебень.
1. [Moon and Space Dimensions (Fabric)](https://www.curseforge.com/minecraft/mc-mods/moon-and-space-dimensions-fabric) - [0.0.16 (02.07.2021)](https://www.curseforge.com/minecraft/mc-mods/moon-and-space-dimensions-fabric/files/all)  
Космос, ракеты, планетки.
1. [Motschen's Puddles](https://www.curseforge.com/minecraft/mc-mods/motschens-puddles) - [1.1.0 (12.05.2021)](https://www.curseforge.com/minecraft/mc-mods/motschens-puddles/files/all)  
Лужи.
1. [Nourish](https://www.curseforge.com/minecraft/mc-mods/nourish) - [1.3.0 (31.05.2021)](https://www.curseforge.com/minecraft/mc-mods/nourish/files/all)  
Баланс хавки.
1. [Patchouli (Fabric)](https://www.curseforge.com/minecraft/mc-mods/patchouli-fabric) - [1.16.4-53 (24.06.2021)](https://www.curseforge.com/minecraft/mc-mods/patchouli-fabric/files/all)  
Книжка с информацией по модам.
1. [Phosphor (Fabric)](https://www.curseforge.com/minecraft/mc-mods/phosphor) - [0.7.2 (30.04.2021)](https://www.curseforge.com/minecraft/mc-mods/phosphor/files/all)  
Оптимизация освещения на сервере.
1. \[L\] [Reborn Core](https://www.curseforge.com/minecraft/mc-mods/reborncore) - [4.7.3 build 136 (30.05.2021)](https://www.curseforge.com/minecraft/mc-mods/reborncore/files/all)  
Либа для Tech Reborn.
1. [Roughly Enough Items Fabric (REI)](https://www.curseforge.com/minecraft/mc-mods/roughly-enough-items) - [5.12.248 (04.06.2021)](https://www.curseforge.com/minecraft/mc-mods/roughly-enough-items/files/all)  
Меню крафтов.
1. [RpgZ](https://www.curseforge.com/minecraft/mc-mods/rpgz) - [0.4.7 (19.05.2021)](https://www.curseforge.com/minecraft/mc-mods/rpgz/files/all)  
Лутание убитых мобов.
1. [Sakura Rosea](https://www.curseforge.com/minecraft/mc-mods/sakura-rosea) - [fabric 1.6.0 (06.12.2020)](https://www.curseforge.com/minecraft/mc-mods/sakura-rosea/files/all)  
Биом сакуры.
1. [Sandwichable](https://www.curseforge.com/minecraft/mc-mods/sandwichable) - [1.2 beta 5 (17.02.2021)](https://www.curseforge.com/minecraft/mc-mods/sandwichable/files/all)  
Механика сэндвичей.
1. [Shelf](https://www.curseforge.com/minecraft/mc-mods/shelf) - [1.2 (25.02.2021)](https://www.curseforge.com/minecraft/mc-mods/shelf/files/all)  
Полка для вещей.
1. [Sodium](https://github.com/CaffeineMC/sodium-fabric) - [0.2.0 (18.06.2021)](https://github.com/CaffeineMC/sodium-fabric/releases)  
Оптимизация рендера, также нужен для Fade In Chunks.
1. [Tech Reborn](https://www.curseforge.com/minecraft/mc-mods/techreborn) - [3.8.4 build 236 (30.05.2021)](https://www.curseforge.com/minecraft/mc-mods/techreborn/files/all)  
Механизмы.
1. [TIS-3D](https://www.curseforge.com/minecraft/mc-mods/tis-3d) - [1.6.2.35 (11.07.2021)](https://www.curseforge.com/minecraft/mc-mods/tis-3d/files/all)  
Низкоуровневое программирование по типу ассемблера.
1. [Tiny Trash Can](https://www.curseforge.com/minecraft/mc-mods/tiny-trash-can) - [1.2.0 (09.02.2021)](https://www.curseforge.com/minecraft/mc-mods/tiny-trash-can/files/all)  
Мусорка.
1. \[L\] [Trinkets (Fabric)](https://www.curseforge.com/minecraft/mc-mods/trinkets-fabric) - [2.6.7 (26.04.2021)](https://www.curseforge.com/minecraft/mc-mods/trinkets-fabric/files/all)  
Либа для The Aether Reborn.

Клиентские моды:

1. [Cull Particles Fabric](https://www.curseforge.com/minecraft/mc-mods/cull-particles-fabric) - [2.0 (04.01.2021)](https://www.curseforge.com/minecraft/mc-mods/cull-particles-fabric/files/all)  
Оптимизация рендера частиц.
1. [EntityCulling](https://www.curseforge.com/minecraft/mc-mods/entityculling) - [fabric 1.3.0 (20.06.2021)](https://www.curseforge.com/minecraft/mc-mods/entityculling/files/all)  
Оптимизация рендера мобов.
1. [Mod Menu](https://www.curseforge.com/minecraft/mc-mods/modmenu) - [1.16.10 (20.07.2021)](https://www.curseforge.com/minecraft/mc-mods/modmenu/files/all)  
Список модов и их настроек.
1. [Numeric Ping](https://www.curseforge.com/minecraft/mc-mods/numericping) - [1.0.2 (12.09.2020)](https://www.curseforge.com/minecraft/mc-mods/numericping/files/all)  
Отображение нормального пинга.
1. [Ok Zoomer](https://www.curseforge.com/minecraft/mc-mods/ok-zoomer) - [4.0.1 (02.09.2020)](https://www.curseforge.com/minecraft/mc-mods/ok-zoomer/files/all)  
Зум.

Серверные моды:

1. [Door(s) Coupling [FABRIC]](https://www.curseforge.com/minecraft/mc-mods/couplings-for-fabric-updated) - [1.3.6 (14.11.2020)](https://www.curseforge.com/minecraft/mc-mods/couplings-for-fabric-updated/files/all)  
Нормальное открытие двойной двери.
1. [EditSign](https://www.curseforge.com/minecraft/mc-mods/edit-sign) - [fabric 2.2.1 (26.04.2021)](https://www.curseforge.com/minecraft/mc-mods/edit-sign/files/all)  
Редактирование табличек.
1. [Fabric Chunk Pregenerator](https://www.curseforge.com/minecraft/mc-mods/chunk-pregenerator-fabric) - [0.3.3 (29.10.2020)](https://www.curseforge.com/minecraft/mc-mods/chunk-pregenerator-fabric/files/all)  
Прегенерация чанков.
1. [FallingTree](https://www.curseforge.com/minecraft/mc-mods/falling-tree) - [fabric 2.11.5 (12.06.2021)](https://www.curseforge.com/minecraft/mc-mods/falling-tree/files/all)  
Быстрая вырубка деревьев.
1. [Fat Experience Orbs](https://www.curseforge.com/minecraft/mc-mods/fat-experience-orbs) - [0.0.9 (02.12.2020)](https://www.curseforge.com/minecraft/mc-mods/fat-experience-orbs/files/all)  
Опыт собирается в одну кучу.
1. [Hypnos](https://www.curseforge.com/minecraft/mc-mods/hypnos) - [0.1.2 (22.04.2021)](https://www.curseforge.com/minecraft/mc-mods/hypnos/files/all)  
Набор процентов спящих для пропуска ночи.
1. [SimpleHarvest](https://www.curseforge.com/minecraft/mc-mods/simpleharvest) - [fabric 1.2.12-26 (27.06.2020)](https://www.curseforge.com/minecraft/mc-mods/simpleharvest/files/all)  
Быстрое собирание урожая.
1. [spark](https://www.curseforge.com/minecraft/mc-mods/spark) - [fabric 1.6.0 beta (04.06.2021)](https://www.curseforge.com/minecraft/mc-mods/spark/files/all)  
График производительности сервера.

\[L\]: Библиотека