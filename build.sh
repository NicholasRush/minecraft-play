#!/bin/bash
rm -rf Build
mkdir Build
mkdir Build/Client
mkdir Build/Client/mods
cp Mods/General/* Build/Client/mods/
cp Mods/Client/* Build/Client/mods/
mkdir Build/Server
mkdir Build/Server/mods
cp Mods/General/* Build/Server/mods/
cp Mods/Server/* Build/Server/mods/